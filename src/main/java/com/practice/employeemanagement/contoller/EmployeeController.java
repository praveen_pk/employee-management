package com.practice.employeemanagement.contoller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.practice.employeemanagement.model.entity.EmployeeEntity;
import com.practice.employeemanagement.model.mapper.EmployeeMapper;
import com.practice.employeemanagement.model.vo.EmployeeVO;
import com.practice.employeemanagement.repository.EmployeeRepository;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepository;

	@Autowired
	private EmployeeMapper employeeMapper;

	@PostMapping(value = { "/register" })
	public ResponseEntity<EmployeeVO> registerEmployee(@RequestBody EmployeeVO employeeVO) {
		EmployeeEntity employeeEntity = employeeMapper.convertVOToEntity(employeeVO);
		employeeRepository.save(employeeEntity);
		return new ResponseEntity<>(employeeMapper.convertEntityToVO(employeeEntity), HttpStatus.OK);
	}

	@GetMapping(value = { "/get/employees" })
	public List<EmployeeVO> getAllEmployees() {
		List<EmployeeEntity> employeeEntityList = employeeRepository.findAll();
		return employeeMapper.convertEntityListToVOList(employeeEntityList);
	}
}
